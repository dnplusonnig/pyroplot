#!/usr/bin/env python2
"""
Python ROOT histogram exporter - A tool for exporting selected ROOT histograms

Run
python pyroexport.py --help
to see the list of command line options.

"""

# TODO:
# - add ability to export 2D and 3D histograms

import sys
from sys import exit # use sys.exit instead of built-in exit (latter raises exception)

import logging
import os
import pyroplot

def exportHists( histos, outputFilePath, infile, overwrite):
    """
    Exports histograms into csv files
    """
    from rootpy.plotting import Hist
    log = logging.getLogger('pyroexport')
    import csv
    # loop over all loaded histograms
    for idx, name in enumerate(sorted(histos.keys())):
        # determine output file name:
        # we want to reproduce the ROOT file folder structure
        # and therefore check the path of the Hist inside the file
        if (len(os.path.dirname(name))>1): # path more than just a slash
            outfile = os.path.join(outputFilePath,
                                   os.path.dirname(name),
                                   os.path.basename(name) + '.csv')
        else:
            outfile = os.path.join(outputFilePath,
                                   os.path.basename(name) + '.csv')
        # do not overwrite any files:
        if os.path.exists(outfile):
            if not overwrite:
                log.error("File exists: {}! Will NOT overwrite it. Use '--overwrite' argument to change this.".format(outfile))
                continue
            else:
                log.warn("Overwriting existing file: {}!.".format(outfile))
        # create folders if needed:
        if not os.path.exists(os.path.dirname(outfile)) and os.path.dirname(outfile):
            os.makedirs(os.path.dirname(outfile))
        # now open the output file:
        log.debug("Writing to file {}".format(outfile))
        with open(outfile, 'wb') as csvfile:
            histexporter = csv.writer(csvfile)
            # write a header first
            histexporter.writerow(["PYROEXPORT HEADER"])
            histexporter.writerow(["RootFile", infile])
            histexporter.writerow(["HistTitle", histos[name].GetTitle()])
            histexporter.writerow(["XAxis", histos[name].GetXaxis().GetTitle()])
            histexporter.writerow(["YAxis", histos[name].GetYaxis().GetTitle()])
            histexporter.writerow(["NBins", histos[name].nbins()])
            histexporter.writerow(["DATA"])
            # loop over Hist bins and dump content into file

            histexporter.writerow(["lower bin edge",
                                   "bin content",
                                   "bin error"])
            for bin in histos[name].bins_range():
                histexporter.writerow([histos[name].xedgesl(bin),
                                       histos[name].GetBinContent(bin),
                                       histos[name].GetBinError(bin)])
            # write the edge of the last bin on the final row
            histexporter.writerow([histos[name].xedgesh(histos[name].nbins())])

        log.info("Exported {}!".format(outfile))

def pyroexport( argv = sys.argv ):
    # Must be done before :py:mod:`rootpy` logs any messages.
    import logging;
    log = logging.getLogger('pyroexport') # set up logging

    try:
        import ROOT
    except ImportError:
        # module failed to load - maybe PYTHONPATH is not set correctly?
        # guess the right path, but that is only possible if ROOTSYS is set:
        if os.environ.get('ROOTSYS') is None:
            print ("ERROR: Could not load the Python ROOT module. Please make sure that your ROOT installation is compiled with Python support and that your PYTHONPATH is set correctly and includes libPyROOT.so")
            exit(1)
        sys.path.append(os.path.join(os.environ.get('ROOTSYS'),"lib"))
        sys.path.append(os.path.join(os.environ.get('ROOTSYS'),"lib","root"))
        # try again:
        try:
            import ROOT
        except ImportError:
            print ("ERROR: Could not load the Python ROOT module. Please make sure that your ROOT installation is compiled with Python support and that your PYTHONPATH is set correctly and includes libPyROOT.so")
            exit(1)

    try:
        import rootpy
    except ImportError:
        # rootpy is not installed; search for one in the pyroplot "pymodules" subdirectory
        # determine (real) path to subdirectory pymodules (relative to current path)
        libdir = os.path.join(os.path.dirname(os.path.abspath(os.path.realpath(__file__))),"pymodules","rootpy")
        # search for any rootpy folders
        import glob
        rootpydirs = glob.glob(libdir+"*")
        if not rootpydirs:
            print ("Error: Could not find the rootpy module. Searched in global search path and in {}!".format(libdir))
        else:
            # add last entry to python search path (subfolder rootpy where the modules are located)
            sys.path.append(rootpydirs[-1])
        # try again loading the module
        try:
            import rootpy
        except ImportError:
            print ("Error: Could not load the rootpy modules. Please install them from http://www.rootpy.org/install.html or using 'pip install rootpy'")
            exit(1)
        except SyntaxError:
            req_version = (2,5)
            cur_version = sys.version_info
            if cur_version < req_version:
                print ("Error: Python version too old: due to its dependency on rootpy, this script requires a Python interpreter version 2.6 or later (installed: {}.{}.{})!".format(cur_version[:3]))
                exit(1)
            print ("Error: Failed to load rootpy module! Possibly incompatible with installed Python version ({}.{}.{})?".format(cur_version[:3]))
            exit(1)

    from rootpy import log; log = log["/pyroexport"]
    ROOT.gROOT.SetBatch(True)
    ROOT.gErrorIgnoreLevel = 1001

    import argparse
    # command line argument parsing
    parser = argparse.ArgumentParser(description="Python ROOT histogram exporter -- a tool for exporting selected ROOT histograms")
    parser.add_argument("-l", "--log-level", default="info", help="Sets the verbosity of log messages where LEVEL is either debug, info, warning or error", metavar="LEVEL")
    parser.add_argument('--select', '-s', action='append', help="Specify regular expression(s) for histogram selection.")
    parser.add_argument('--invert', '-v', action='store_true', default=False, help="Select only all non-matching histograms.")
    parser.add_argument("--selection-from-file", help="Load list of regular expressions for histogram selection from file (plain text file, one reg ex per line).", metavar="FILE")

    parser.add_argument("-o","--output", default="./", help="Output path. The path will be created if needed.", metavar="PATH")
    parser.add_argument("--overwrite", action="store_true", default=False, help="Overwrite output file if already existing.")
    parser.add_argument("--with-2D","-2D", action="store_true", default=False, help="Also loads TH2-type histograms.")
    parser.add_argument("--with-3D","-3D", action="store_true", default=False, help="Also loads TH3-type and Profile2D-type histograms, implies --with-2D.")
    parser.add_argument("--list-only", "--list", action="store_true", default=False, help="Do not export histograms but only list objects in ROOT file(s) and indicate which ones would be selected.")
    parser.add_argument("--strict", action="store_true", default=False, help="Require the selection to match the full histogram path and name (with implied '^' and '$') instead of only a partial match.")
    parser.add_argument("file", help="The file to be processed")
    # parse the arguments
    args = parser.parse_args(argv)
    # set the logging level
    numeric_level = getattr(logging, "INFO", None) # default: INFO messages and above
    if args.log_level:
        # Convert log level to upper case to allow the user to specify --log-level=DEBUG or --log-level=debug
        numeric_level = getattr(logging, args.log_level.upper(), None)
        if not isinstance(numeric_level, int):
            log.error('Invalid log level: %s' % args.log_level)
            exit(2)
    log.setLevel(numeric_level)
    # apply same log level to routines called in pyroplot
    logpyroplot = logging.getLogger('pyroplot')
    logpyroplot.setLevel(numeric_level)

    log.debug( "Command line arguments used: %s ", args )

    log.debug("Using rootpy %s from %s"%(rootpy.__version__,rootpy.__file__))

    # laod and combine all specified reg ex
    regexs = []
    # first from file
    if args.selection_from_file:
        f = open(args.selection_from_file, 'r')
        try:
            lines = f.read().splitlines()
            for line in lines:
                if line: # test if line is not empty (would match anything)
                    log.debug("Loading reg ex from file " + args.selection_from_file 
                              + ": '" + line +"'")
                    regexs.append(line)
        finally:
            f.close()
    if args.select:
        for arg in args.select:
            log.debug("Using reg ex from command line: " + arg)
            regexs.append(arg)
    # still nothing to select? use default
    if not regexs:
        import inspect
        filepath = os.path.join(os.path.dirname(os.path.abspath(os.path.realpath(__file__))),"default.sel")
        try:
            f = open(filepath, 'r')
            try:
                lines = f.read().splitlines()
                for line in lines:
                    if line: # test if line is not empty (would match anything)
                        log.debug("Loading reg ex from file " + filepath + ": '" + line +"'")
                        regexs.append(line)
            finally:
                f.close()
        except IOError:
            log.warn("Could not find the file with the default selection ('"+filepath+"'), will use default of '.*' (select all)")
            regexs.append('.*')

    # parse output path: strip file name (if specified)
    outputFilePath = os.path.dirname(args.output)

    histos = {} # our histograms: dictionary with the hists full path in the root file as key
    selectedHistos = []
    # only search for matching histo names on first iteration if doing a comparison between files
    selectedHistos = pyroplot.findHistogramsInFile(args.file, regexs, args.strict, args.invert, args.list_only)
    if not args.list_only:
        histos = pyroplot.loadHistogramsFromFile( args.file , selectedHistos, args.with_2D, args.with_3D) # append to main histo list
    if histos:
        log.info("Input file read. %d histograms matched selection criteria and were loaded"%(len(histos) ))
        exportHists(histos, outputFilePath, args.file, args.overwrite)

    log.info("done")

if __name__ == "__main__":
    pyroexport( sys.argv[1:])
